<!-- This file is used to store sidebar items, starting with Backpack\Base 0.9.0 -->
<li><a href="{{ backpack_url('dashboard') }}"><i class="fa fa-dashboard"></i>
        <span>{{ trans('backpack::base.dashboard') }}</span></a></li>
<li><a href="{{ backpack_url('elfinder') }}"><i class="fa fa-files-o"></i>
        <span>{{ trans('backpack::crud.file_manager') }}</span></a></li>
@role('admin')
<li><a href='{{ backpack_url('user') }}'><i class='fa fa-tag'></i> <span>Пользователи</span></a></li>
@endrole
<li><a href='{{ backpack_url('category') }}'><i class='fa fa-tag'></i> <span>Категории</span></a></li>
<li><a href='{{ backpack_url('shop') }}'><i class='fa fa-tag'></i> <span>Магазины</span></a></li>
<li><a href='{{ backpack_url('transaction') }}'><i class='fa fa-tag'></i> <span>Транзакции</span></a></li>
<li class="treeview">
    <a href="#"><i class="fa fa-globe"></i> <span>Translations</span> <i class="fa fa-angle-left pull-right"></i></a>
    <ul class="treeview-menu">
        <li><a href="{{ url(config('backpack.base.route_prefix', 'admin').'/language') }}"><i
                        class="fa fa-flag-checkered"></i> Languages</a></li>
        <li><a href="{{ url(config('backpack.base.route_prefix', 'admin').'/language/texts') }}"><i
                        class="fa fa-language"></i> Site texts</a></li>
    </ul>
</li>

<li><a href='{{ backpack_url('setting') }}'><i class='fa fa-settings'></i><span>Настройки</span></a></li>
@role('admin')
<li><a href='{{ backpack_url('notification') }}'><i class='fa fa-notification'></i><span>Push-уведомления</span></a>
</li>
@endrole