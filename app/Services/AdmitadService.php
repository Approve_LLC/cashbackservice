<?php

namespace App\Services;

use Carbon\Carbon;
use Admitad;

/**
 * Class AdmitadService
 * @package App\Services
 */
class AdmitadService implements AdServiceInterface
{

    /**
     * @return object|null
     */
    public function getToken(): ?object
    {
        $api = new Admitad\Api\Api();
        $clientId = config('app.client_id');
        $clientPassword = config('app.client_password');
        $scope = config('app.scope');
        $response = $api->authorizeClient(
            $clientId,
            $clientPassword,
            $scope
        );

        if (empty($response)) {
            return null;
        }

        return $response;
    }

    /**
     * @param string $expireAt
     * @return bool
     */
    public function tokenExpired(string $expireAt): bool
    {
        if (Carbon::now() < Carbon::createFromTimestamp($expireAt)) {
            return false;
        }

        return true;
    }

    /**
     * @param string $refreshToken
     * @return array|null
     */
    public function refreshToken(string $refreshToken): ?object
    {
        $api = new Admitad\Api\Api();
        $clientId = config('app.client_id');
        $clientPassword = config('app.client_password');
        $response = $api->refreshToken($clientId, $clientPassword, $refreshToken);

        if(empty($response)) {
            return null;
        }

        return $response;
    }

}
