<?php

namespace App\Services;

use App\Models\Notification as NotificationModel;
use Admitad;
use Kreait\Firebase\Messaging\CloudMessage;
use Kreait\Firebase\ServiceAccount;
use Kreait\Firebase\Factory;
use Kreait\Firebase\Messaging\Notification;

/**
 * Class NotificationService
 * @package App\Services
 */
class NotificationService
{
    const TYPE_TOPIC = 'topic';
    const TYPE_TOKEN = 'token';
    const TYPE_CONDITION = 'condition';

    /**
     * @param int $id
     * @param string $type
     * @param string $targetString
     * @return array
     */
    public function sendNotification(int $id,
                                     string $type = self::TYPE_TOKEN,
                                     string $targetString = ''
    ): array
    {
        $data = NotificationModel::where(['id' => $id])->first();
        $notification = Notification::create($data->title, $data->body);
        $message = CloudMessage::withTarget($type, $targetString)
            ->withNotification($notification);
        $serviceAccount = ServiceAccount::fromJsonFile(__DIR__ . '/../../config/cashback-232317-firebase-adminsdk-2ftol-3a4c5b2b06.json');
        $firebase = (new Factory)
            ->withServiceAccount($serviceAccount)
            ->create();
        $messaging = $firebase->getMessaging();

        return $messaging->send($message);
    }

}
