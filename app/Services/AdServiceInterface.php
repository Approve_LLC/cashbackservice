<?php

namespace App\Services;

/**
 * Interface AdServiceInterface
 * @package App\Services
 */
interface AdServiceInterface
{
    /**
     * @return string|null
     */
    public function getToken(): ?object;

    /**
     * @param string $expireAt
     * @return bool
     */
    public function tokenExpired(string $expireAt): bool;

    /**
     * @param string $refreshToken
     * @return string|null
     */
    public function refreshToken(string $refreshToken): ?object;
}
