<?php

namespace App\Models;

use Backpack\CRUD\CrudTrait;
use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\Category
 *
 * @property int $id
 * @property int $parent_id
 * @property string $title
 * @property string $url
 * @property int $count
 * @property int $pages
 * @property string $created_at
 * @property string $updated_at
 * @property Category $category
 * @property \Illuminate\Database\Eloquent\Collection|\App\Models\Shop[] $shops
 */
class Category extends Model
{
    use CrudTrait;

    protected $table = 'categories';

    protected $guarded = ['id'];

    protected $fillable = ['parent_id', 'title', 'url', 'count', 'pages', 'created_at', 'updated_at'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function category()
    {
        return $this->belongsTo('App\Models\Category', 'parent_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function shops()
    {
        return $this->belongsToMany('App\Models\Shop');
    }
}
