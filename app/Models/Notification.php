<?php

namespace App\Models;

use Backpack\CRUD\CrudTrait;
use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\Notification
 *
 * @property int $id
 * @property string $title
 * @property string $body
 * @property string $created_at
 * @property string $updated_at
 */
class Notification extends Model
{
    use CrudTrait;

    protected $table = 'notifications';

    protected $guarded = ['id'];

    protected $fillable = ['title', 'body', 'created_at', 'updated_at'];

}
