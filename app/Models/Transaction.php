<?php

namespace App\Models;

use Backpack\CRUD\CrudTrait;
use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\Transaction
 *
 * @property int $id
 * @property int $user_id
 * @property float $value
 * @property int $status
 * @property string $created_at
 * @property string $updated_at
 * @property User $user
 */
class Transaction extends Model
{
    use CrudTrait;

    const STATUS_PENDING = 0;
    const STATUS_SUCCESS = 1;
    const STATUS_REJECT = 2;

    public static $statuses = [
        self::STATUS_PENDING => 'В процессе',
        self::STATUS_SUCCESS => 'Готово',
        self::STATUS_REJECT => 'Отказ',
    ];

    protected $table = 'transactions';

    protected $fillable = ['user_id', 'value', 'status', 'created_at', 'updated_at'];

    protected $guarded = ['id'];

    public function user()
    {
        return $this->belongsTo('App\Models\User');
    }
}
