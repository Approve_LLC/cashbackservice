<?php

namespace App\Models;

use Backpack\CRUD\CrudTrait;
use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\Shop
 *
 * @property int $id
 * @property string $title
 * @property string $cash
 * @property string $filename
 * @property string $created_at
 * @property string $updated_at
 * @property \Illuminate\Database\Eloquent\Collection|\App\Models\Category[] $categories
 */
class Shop extends Model
{
    use CrudTrait;

    protected $table = 'shops';

    protected $fillable = ['title', 'cash', 'filename', 'created_at', 'updated_at'];

    protected $guarded = ['id'];

    public function categories()
    {
        return $this->belongsToMany('App\Models\Category');
    }

}
