<?php

namespace App\Models;

use Backpack\CRUD\CrudTrait;
use Illuminate\Notifications\Notifiable;
use Laravel\Passport\HasApiTokens;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Zizaco\Entrust\Traits\EntrustUserTrait;

/**
 * App\Models\User
 *
 * @property int $id
 * @property int $referral_id
 * @property string $name
 * @property string $email
 * @property string $email_verified_at
 * @property string $password
 * @property string $remember_token
 * @property string $created_at
 * @property string $updated_at
 * @property string $referral_code
 * @property int $vk_id
 * @property int $google_id
 * @property int $fb_id
 * @property string $gender
 * @property string $phone
 * @property string $was_born
 * @property string $device_token
 * @property string $role
 * @property User $user
 * @property Transaction[] $transactions
 * @property \Illuminate\Database\Eloquent\Collection|\Laravel\Passport\Client[] $clients
 * @property \Illuminate\Notifications\DatabaseNotificationCollection|\Illuminate\Notifications\DatabaseNotification[] $notifications
 * @property \App\Models\User|null $referral
 * @property \Illuminate\Database\Eloquent\Collection|\Laravel\Passport\Token[] $tokens
 */
class User extends Authenticatable
{
    const ROLE_ADMIN = 'admin';
    const ROLE_MODERATOR = 'moderator';
    const ROLE_USER = 'user';

    use HasApiTokens, Notifiable, CrudTrait, EntrustUserTrait;

    protected $guarded = ['id'];
    protected $fillable = [
        'referral_id', 'name',
        'email', 'email_verified_at',
        'password', 'remember_token',
        'created_at', 'updated_at',
        'referral_code', 'vk_id',
        'google_id', 'fb_id',
        'gender', 'phone',
        'was_born', 'device_token',
        'role'
    ];
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function referral()
    {
        return $this->belongsTo('App\Models\User');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function transactions()
    {
        return $this->hasMany('App\Models\Transaction');
    }
}
