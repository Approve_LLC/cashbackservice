<?php

namespace App\Models;

use Backpack\CRUD\CrudTrait;
use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\Setting
 *
 * @property int $id
 * @property string $name
 * @property string $value
 * @property string $created_at
 * @property string $updated_at
 */
class Setting extends Model
{
    use CrudTrait;

    protected $table = 'settings';

    protected $fillable = ['name', 'value', 'created_at', 'updated_at'];

    protected $guarded = ['id'];

    /**
     * Описание кнопки для backpack admin'а
     * @param bool $crud
     * @return string
     */
    public function sendAndroidPush($crud = false)
    {
        if ($this->name == 'android_notification') {
            return '<a class="btn btn-xs btn-success"
        target="_blank"
        href="/send-android" 
        data-toggle="tooltip" title="Send it!">
        <i class="fa fa-envelope"></i> Send android
        </a>';
        }
    }

    /**
     * Описание кнопки для backpack admin'а
     * @param bool $crud
     * @return string
     */
    public function sendIOSPush($crud = false)
    {
        if ($this->name == 'ios_notification') {
            return '<a class="btn btn-xs btn-primary"
        target="_blank"
        href="/send-ios" 
        data-toggle="tooltip" title="Send it!">
        <i class="fa fa-envelope"></i> Send ios
        </a>';
        }
    }

}
