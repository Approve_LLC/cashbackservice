<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;

/**
 * Class AdmitadServiceProvider
 * @package App\Providers
 */
class AdmitadServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }

    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind(
            'App\Services\AdServiceInterface',
            'App\Services\AdmitadService'
        );
    }
}
