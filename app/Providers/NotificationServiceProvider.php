<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;

/**
 * Class NotificationServiceProvider
 * @package App\Providers
 */
class NotificationServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }

    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind(
            'App\Repositories\NotificationService'
        );
    }
}
