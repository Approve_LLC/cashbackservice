<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;

/**
 * Class UserRepositoryProvider
 * @package App\Providers
 */
class UserRepositoryProvider extends ServiceProvider
{
    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }

    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind(
            'App\Repositories\UserRepository'
        );
    }
}
