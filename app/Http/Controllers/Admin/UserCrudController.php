<?php

namespace App\Http\Controllers\Admin;

use Backpack\CRUD\app\Http\Controllers\CrudController;

// VALIDATION: change the requests to match your own file names if you need form validation
use App\Http\Requests\UserRequest as StoreRequest;
use App\Http\Requests\UserRequest as UpdateRequest;
use Illuminate\Support\Facades\Auth;
use Zizaco\Entrust\Entrust;

/**
 * Class UserCrudController
 * @package App\Http\Controllers\Admin
 * @property-read CrudPanel $crud
 */
class UserCrudController extends CrudController
{
    public function setup()
    {
        /*
        |--------------------------------------------------------------------------
        | CrudPanel Basic Information
        |--------------------------------------------------------------------------
        */
        $this->crud->setModel('App\Models\User');
        $this->crud->setRoute(config('backpack.base.route_prefix') . '/user');
        $this->crud->setEntityNameStrings('user', 'users');

        $this->crud->setColumns(['id', 'name', 'email', 'created_at', 'updated_at']);

        $this->crud->addField([
            'name' => 'name',
            'type' => 'text',
            'label' => "Имя пользователя"
        ]);

        $this->crud->addField([
            'name' => 'email',
            'type' => 'email',
            'label' => "Почта"
        ]);

        $this->crud->addField([
            'name' => 'password',
            'type' => 'password',
            'label' => "Пароль"
        ]);

        $this->crud->addField([
            'name' => 'referral_code',
            'type' => 'text',
            'label' => "Реферальный код"
        ]);

        $this->crud->addField([
            'label' => "Реферал",
            'type' => 'select',
            'name' => 'referral_id',
            'entity' => 'referral',
            'attribute' => 'name',
            'model' => "App\\Models\\User"
        ]);


        if(!Auth::user()->hasRole('admin')) {
            $this->crud->denyAccess(['update','list', 'create', 'delete']);
        }

        /*
        |--------------------------------------------------------------------------
        | CrudPanel Configuration
        |--------------------------------------------------------------------------
        */

        // TODO: remove setFromDb() and manually define Fields and Columns
        //$this->crud->setFromDb();

        // add asterisk for fields that are required in UserRequest
        $this->crud->setRequiredFields(StoreRequest::class, 'create');
        $this->crud->setRequiredFields(UpdateRequest::class, 'edit');
    }

    public function store(StoreRequest $request)
    {
        // your additional operations before save here
        $this->handlePasswordInput($request);
        $redirect_location = parent::storeCrud($request);
        // your additional operations after save here
        // use $this->data['entry'] or $this->crud->entry
        return $redirect_location;
    }

    public function update(UpdateRequest $request)
    {
        // your additional operations before save here
        $redirect_location = parent::updateCrud($request);
        // your additional operations after save here
        // use $this->data['entry'] or $this->crud->entry
        return $redirect_location;
    }

    protected function handlePasswordInput(StoreRequest $request)
    {
        // Remove fields not present on the user.
        $request->request->remove('password_confirmation');
        // Encrypt password if specified.
        if ($request->input('password')) {
            $request->request->set('password', bcrypt($request->input('password')));
        } else {
            $request->request->remove('password');
        }
    }
}
