<?php

namespace App\Http\Controllers\Admin;

use Backpack\CRUD\app\Http\Controllers\CrudController;

// VALIDATION: change the requests to match your own file names if you need form validation
use App\Http\Requests\ShopRequest as StoreRequest;
use App\Http\Requests\ShopRequest as UpdateRequest;
use Illuminate\Support\Facades\Auth;

/**
 * Class ShopCrudController
 * @package App\Http\Controllers\Admin
 * @property-read CrudPanel $crud
 */
class ShopCrudController extends CrudController
{
    public function setup()
    {
        /*
        |--------------------------------------------------------------------------
        | CrudPanel Basic Information
        |--------------------------------------------------------------------------
        */
        $this->crud->setModel('App\Models\Shop');
        $this->crud->setRoute(config('backpack.base.route_prefix') . '/shop');
        $this->crud->setEntityNameStrings('shop', 'shops');

        /*
        |--------------------------------------------------------------------------
        | CrudPanel Configuration
        |--------------------------------------------------------------------------
        */

        // TODO: remove setFromDb() and manually define Fields and Columns
        $this->crud->setFromDb();

        $this->crud->denyAccess(['update','list', 'create', 'delete']);

        if(Auth::user()->hasRole('admin')) {
            $this->crud->allowAccess(['update','list', 'create', 'delete']);
        }
        if(Auth::user()->hasRole('moderator')) {
            $this->crud->allowAccess(['list']);
        }

        // add asterisk for fields that are required in ShopRequest
        $this->crud->setRequiredFields(StoreRequest::class, 'create');
        $this->crud->setRequiredFields(UpdateRequest::class, 'edit');
    }

    public function store(StoreRequest $request)
    {
        // your additional operations before save here
        $redirect_location = parent::storeCrud($request);
        // your additional operations after save here
        // use $this->data['entry'] or $this->crud->entry
        return $redirect_location;
    }

    public function update(UpdateRequest $request)
    {
        // your additional operations before save here
        $redirect_location = parent::updateCrud($request);
        // your additional operations after save here
        // use $this->data['entry'] or $this->crud->entry
        return $redirect_location;
    }
}
