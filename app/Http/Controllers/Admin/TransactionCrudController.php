<?php

namespace App\Http\Controllers\Admin;

use App\Models\Transaction;
use Backpack\CRUD\app\Http\Controllers\CrudController;

// VALIDATION: change the requests to match your own file names if you need form validation
use App\Http\Requests\TransactionRequest as StoreRequest;
use App\Http\Requests\TransactionRequest as UpdateRequest;
use Illuminate\Support\Facades\Auth;

/**
 * Class TransactionCrudController
 * @package App\Http\Controllers\Admin
 * @property-read CrudPanel $crud
 */
class TransactionCrudController extends CrudController
{
    public function setup()
    {
        /*
        |--------------------------------------------------------------------------
        | CrudPanel Basic Information
        |--------------------------------------------------------------------------
        */
        $this->crud->setModel('App\Models\Transaction');
        $this->crud->setRoute(config('backpack.base.route_prefix') . '/transaction');
        $this->crud->setEntityNameStrings('transaction', 'transactions');

        /*
        |--------------------------------------------------------------------------
        | CrudPanel Configuration
        |--------------------------------------------------------------------------
        */

        $this->crud->setColumns(['id', 'user_id', 'value']);
        $this->crud->addColumn([
            'name' => 'status',
            'label' => 'Статус',
            'type' => 'select_from_array',
            'options' => [
                Transaction::STATUS_PENDING => Transaction::$statuses[Transaction::STATUS_PENDING],
                Transaction::STATUS_SUCCESS => Transaction::$statuses[Transaction::STATUS_SUCCESS],
                Transaction::STATUS_REJECT => Transaction::$statuses[Transaction::STATUS_REJECT]
            ]
        ]);

        // TODO: remove setFromDb() and manually define Fields and Columns
        $this->crud->addField([
            'label' => 'Пользователь',
            'type' => 'select',
            'name' => 'user_id',
            'entity' => 'user',
            'attribute' => 'name',
            'model' => "App\\Models\\User"
        ]);

        $this->crud->addField([
            'name' => 'value',
            'label' => 'Значение',
            'type' => 'number',
        ]);
        $this->crud->addField([
            'name' => 'status',
            'label' => 'Статус',
            'type' => 'select_from_array',
            'options' => [
                Transaction::STATUS_PENDING => Transaction::$statuses[Transaction::STATUS_PENDING],
                Transaction::STATUS_SUCCESS => Transaction::$statuses[Transaction::STATUS_SUCCESS],
                Transaction::STATUS_REJECT => Transaction::$statuses[Transaction::STATUS_REJECT]
            ],
            'allows_null' => false,
            'default' => 0,
        ]);

        $this->crud->denyAccess(['update','list', 'create', 'delete']);

        if(Auth::user()->hasRole('admin')) {
            $this->crud->allowAccess(['update','list', 'create', 'delete']);
        }
        if(Auth::user()->hasRole('moderator')) {
            $this->crud->allowAccess(['list']);
        }

        // add asterisk for fields that are required in TransactionRequest
        $this->crud->setRequiredFields(StoreRequest::class, 'create');
        $this->crud->setRequiredFields(UpdateRequest::class, 'edit');
    }

    public function store(StoreRequest $request)
    {
        // your additional operations before save here
        $redirect_location = parent::storeCrud($request);
        // your additional operations after save here
        // use $this->data['entry'] or $this->crud->entry
        return $redirect_location;
    }

    public function update(UpdateRequest $request)
    {
        // your additional operations before save here
        $redirect_location = parent::updateCrud($request);
        // your additional operations after save here
        // use $this->data['entry'] or $this->crud->entry
        return $redirect_location;
    }
}
