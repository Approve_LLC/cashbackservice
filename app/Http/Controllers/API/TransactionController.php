<?php

namespace App\Http\Controllers\API;

use App\Http\Resources\TransactionResource;
use App\Models\Transaction;
use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

/**
 * Class TransactionController
 * @package App\Http\Controllers\API
 */
class TransactionController extends Controller
{

    /**
     * @param Request $request
     * @return array
     * @throws \Illuminate\Validation\ValidationException
     */
    public function create(Request $request)
    {
        $response = [];
        $this->validate($request, [
            'value' => 'numeric|required',
        ]);
        $transaction = new Transaction($request->input());
        $transaction->user_id = $request->user()->id;
        $transaction->status = Transaction::STATUS_PENDING;
        $transaction->save();
        $response['user'] = new TransactionResource($transaction);
        $user = User::query()->find($transaction->user_id);
        $referralUser = $user->referral;
        if (!empty($referralUser)) {
            $refTransaction = new Transaction();
            $refTransaction->value = ($transaction->value * config('app.cashback_percent')) / 100;
            $refTransaction->user_id = $referralUser->id;
            $refTransaction->status = Transaction::STATUS_PENDING;
            $refTransaction->save();
            $response['referral_user'] = new TransactionResource($refTransaction);
        }

        return $response;
    }

    /**
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function delete($id)
    {
        Transaction::where(['id' => $id])->delete();

        return response()->json([
            'message' => 'Транзакция удалена!'
        ], 200);
    }
}
