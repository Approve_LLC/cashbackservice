<?php

namespace App\Http\Controllers\API;

use App\Http\Resources\CategoryResource;
use App\Http\Resources\ShopResource;
use App\Models\Category;
use App\Models\Notification;
use App\Services\NotificationService;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

/**
 * Class NotificationController
 * @package App\Http\Controllers\API
 */
class NotificationController extends Controller
{

    private $notificationService;

    public function __construct(
        NotificationService $notificationService
    )
    {
        $this->notificationService = $notificationService;
    }

    /**
     * @param Request $request
     * @param int $id
     * @return string
     */
    public function send(Request $request, int $id): string
    {
        $users = $request->users ?? [];
        $topic = $request->topic ?? '';
        $condition = $request->condition ?? '';
        $messages = [];

        if (!empty($topic)) {
            $messages['topic'] = $this->notificationService->sendNotification(
                $id,
                NotificationService::TYPE_TOPIC,
                $topic
            );
        }
        if (!empty($condition)) {
            $messages['condition'] = $this->notificationService->sendNotification(
                $id,
                NotificationService::TYPE_CONDITION,
                $condition
            );
        }
        if (!empty($users)) {
            foreach ($users as $user) {
                $messages['users'][$user->id] = $this->notificationService->sendNotification(
                    $id,
                    NotificationService::TYPE_TOKEN,
                    $user->deviceToken
                );
            }
        }

        return response()->json([
            'message' => $messages
        ], 200);
    }
}
