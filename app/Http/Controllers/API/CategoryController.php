<?php

namespace App\Http\Controllers\API;

use App\Http\Resources\CategoryResource;
use App\Http\Resources\ShopResource;
use App\Models\Category;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

/**
 * @group Category management
 *
 * APIs for managing categories
 */
class CategoryController extends Controller
{

    /**
     * @return \Illuminate\Http\Resources\Json\AnonymousResourceCollection
     */
    public function index()
    {
        $categories = Category::all();
        $collection = CategoryResource::collection($categories);

        return $collection;
    }

    /**
     * @param $id
     * @return CategoryResource
     */
    public function show($id)
    {
        $category = Category::where(['id' => $id])->first();

        return new CategoryResource($category);
    }

    /**
     * @param $categoryId
     * @return \Illuminate\Http\Resources\Json\AnonymousResourceCollection
     */
    public function listByCategory($categoryId)
    {
        $category = Category::find($categoryId);
        $shops = $category->shops;
        $collection = ShopResource::collection($shops);

        return $collection;
    }

    /**
     * @param Request $request
     * @return CategoryResource
     * @throws \Illuminate\Validation\ValidationException
     */
    public function create(Request $request)
    {
        $this->validate($request, [
            'title' => 'required|string',
            'url' => 'string|unique:categories',
            'count' => 'integer',
            'pages' => 'integer',
            'parent_id' => 'integer',
        ]);
        $category = new Category($request->input());
        $category->save();

        return new CategoryResource($category);
    }
}
