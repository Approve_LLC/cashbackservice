<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Http\Resources\UserResource;
use App\Mail\EmailLogin;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Carbon\Carbon;
use App\Models\User;
use Illuminate\Support\Facades\Mail;
use Admitad;
use Laravel\Socialite\Facades\Socialite;

/**
 * @group Auth management
 *
 * APIs for auth
 */
class AuthController extends Controller
{
    /**
     * @param Request $request
     * @return UserResource
     * @throws \Illuminate\Validation\ValidationException
     */
    public function signup(Request $request)
    {
        $this->validate($request, [
            'name' => 'required|string',
            'email' => 'required|string|email|unique:users',
            'password' => 'required|string|confirmed',
            'device_token' => 'required|string|unique:users'
        ]);
        $referralId = null;
        if(!empty($request->referral_code)){
            $user = User::where(['referral_code' => $request->referral_code])->first();
            $referralId = !empty($user) ? $user->id : null;
        }
        $user = new User([
            'name' => $request->name,
            'email' => $request->email,
            'password' => bcrypt($request->password),
            'referral_id' => $referralId,
            'device_token' => $request->device_token,
        ]);
        $user->save();

        return new UserResource($user);
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     * @throws \Illuminate\Validation\ValidationException
     */
    public function login(Request $request)
    {
        $this->validate($request, [
            'email' => 'required|string|email',
            'password' => 'required|string',
            'remember_me' => 'boolean'
        ]);
        $credentials = request(['email', 'password']);
        if (!Auth::attempt($credentials)){
            return response()->json([
                'message' => 'Unauthorized'
            ], 401);
        }
        $user = $request->user();
        $tokenResult = $user->createToken('Personal Access Token');
        $token = $tokenResult->token;
        if ($request->remember_me) {
            $token->expires_at = Carbon::now()->addWeeks(1);
        }
        $token->save();

        return response()->json([
            'access_token' => $tokenResult->accessToken,
            'token_type' => 'Bearer',
            'expires_at' => Carbon::parse(
                $tokenResult->token->expires_at
            )->toDateTimeString()
        ], 200);
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function logout(Request $request)
    {
        $request->user()->token()->revoke();

        return response()->json([
            'message' => 'Успешно.'
        ], 200);
    }

    /**
     * @param Request $request
     * @return UserResource
     */
    public function user(Request $request)
    {
        return new UserResource($request->user());
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     * @throws \Illuminate\Validation\ValidationException
     */
    public function loginByMail(Request $request)
    {
        $this->validate($request, [
            'email' => 'required|string|email',
            'link' => 'required|string'
        ]);

        $result = Mail::to($request->email)
           // ->subject('Login in')
            ->send(new EmailLogin($request->link));

        return response()->json([
            'message' => 'Успешно.'
        ], 200);
    }

    public function loginByMailCallback(Request $request)
    {
        $userFb = Socialite::driver('facebook')->stateless()->user();
        $userId = $userFb->id;
        $user = User::where(['fb_id' => $userId])->first();
        if (empty($user)) {
            $user = new User();
            $user->email = $userFb->email??"$userId@fb.com";
            $user->name = $userFb->name;
            $user->fb_id = $userId;
            $user->password = bcrypt(uniqid());
            $user->save();
        }
        $tokenResult = $user->createToken('Personal Access Token');
        $token = $tokenResult->token;
        $token->expires_at = Carbon::now()->addWeeks(1);
        $token->save();

        return response()->json([
            'access_token' => $tokenResult->accessToken,
            'token_type' => 'Bearer',
            'expires_at' => Carbon::parse(
                $tokenResult->token->expires_at
            )->toDateTimeString()
        ], 200);
    }

    /**
     * @return mixed
     */
    public function redirectToFbProvider()
    {
        return Socialite::driver('facebook')->stateless()->redirect()->getTargetUrl();
    }

    /**
     * @return \Illuminate\Http\JsonResponse
     */
    public function handleProviderFbCallback()
    {
        $userFb = Socialite::driver('facebook')->stateless()->user();
        $userId = $userFb->id;
        $user = User::where(['fb_id' => $userId])->first();
        if (empty($user)) {
            $user = new User();
            $user->email = $userFb->email??"$userId@fb.com";
            $user->name = $userFb->name;
            $user->fb_id = $userId;
            $user->password = bcrypt(uniqid());
            $user->save();
        }
        $tokenResult = $user->createToken('Personal Access Token');
        $token = $tokenResult->token;
        $token->expires_at = Carbon::now()->addWeeks(1);
        $token->save();

        return response()->json([
            'access_token' => $tokenResult->accessToken,
            'token_type' => 'Bearer',
            'expires_at' => Carbon::parse(
                $tokenResult->token->expires_at
            )->toDateTimeString()
        ], 200);
    }

    /**
     * Запрос на авторизацию Вконтакте.
     *
     * Здесь делаем запрос на урл авторизации Вконтакте.
     *
     * @response {
     *  "":"Ссылка на авторизацию Вконтакте"
     * }
     *
     * @return mixed
     */
    public function redirectToVkProvider()
    {
        return Socialite::driver('vkontakte')->scopes(['email'])->stateless()->redirect()->getTargetUrl();
    }

    /**
     * Callback-функция
     *
     * Callback-функция, на которую нас перебрасывает после авторизации Вконтакте.
     * Здесь создается пользователь, если его нет и выдается access_token.
     *
     * @response {
     *  "access_token": "eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiIsINjINNtd3h1o",
     *  "token_type": "Bearer",
     *  "expires_at": "2019-02-03 20:52:35"
     * }
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function handleProviderVkCallback()
    {
        $userVk = Socialite::driver('vkontakte')->stateless()->user();
        $userId = $userVk->accessTokenResponseBody['user_id'];
        $user = User::where(['vk_id' => $userId])->first();
        if (empty($user)) {
            $user = new User();
            //не придумал более надежного варианта для генерации рандомной строки
            $user->email = $userVk->accessTokenResponseBody['email']??"$userId@vkontakte.ru";
            $user->name = $userVk->name;
            $user->vk_id = $userId;
            $user->password = bcrypt(uniqid());
            $user->save();
        }
        $tokenResult = $user->createToken('Personal Access Token');
        $token = $tokenResult->token;
        $token->expires_at = Carbon::now()->addWeeks(1);
        $token->save();

        return response()->json([
            'access_token' => $tokenResult->accessToken,
            'token_type' => 'Bearer',
            'expires_at' => Carbon::parse(
                $tokenResult->token->expires_at
            )->toDateTimeString()
        ], 200);
    }

    /**
     * Запрос на авторизацию Гугл.
     *
     * Здесь делаем запрос на урл авторизации Гугл.
     *
     * @response {
     *  "":"Ссылка на авторизацию в Гугл"
     * }
     *
     * @return mixed
     */
    public function redirectToGoogleProvider()
    {
        return Socialite::driver('google')->stateless()->redirect()->getTargetUrl();
    }

    /**
     * Callback-функция
     *
     * Callback-функция, на которую нас перебрасывает после авторизации Гугл.
     * Здесь создается пользователь, если его нет и выдается access_token.
     *
     * @response {
     *  "access_token": "eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiIsINjINNtd3h1o",
     *  "token_type": "Bearer",
     *  "expires_at": "2019-02-03 20:52:35"
     * }
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function handleProviderGoogleCallback()
    {
        $userGoogle = Socialite::driver('google')->stateless()->user();
        $userId = $userGoogle->id;
        $user = User::where(['google_id' => $userId])->first();
        if (empty($user)) {
            $user = new User();
            $user->email = $userGoogle->email??"$userId@google.com";
            $user->name = $userGoogle->name;
            $user->google_id = $userId;
            $user->password = bcrypt(uniqid());
            $user->save();
        }
        $tokenResult = $user->createToken('Personal Access Token');
        $token = $tokenResult->token;
        $token->expires_at = Carbon::now()->addWeeks(1);
        $token->save();

        return response()->json([
            'access_token' => $tokenResult->accessToken,
            'token_type' => 'Bearer',
            'expires_at' => Carbon::parse(
                $tokenResult->token->expires_at
            )->toDateTimeString()
        ], 200);
    }
}
