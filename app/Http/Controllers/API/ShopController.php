<?php

namespace App\Http\Controllers\API;

use App\Http\Resources\ShopResource;
use App\Models\Shop;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

/**
 * @group Shop management
 *
 * APIs for managing shops
 */
class ShopController extends Controller
{

    /**
     * @param Request $request
     * @return \Illuminate\Http\Resources\Json\AnonymousResourceCollection
     */
    public function index(Request $request)
    {
        $all = $request->input('all');
        if (!empty($all)) {
            $shops = Shop::all();
        } else {
            $shops = Shop::paginate($request->input('per_page'));
        }
        $collection = ShopResource::collection($shops);

        return $collection;
    }

    /**
     * @param $id
     * @return ShopResource
     */
    public function show($id)
    {
        $shop = Shop::where(['id' => $id])->first();

        return new ShopResource($shop);
    }
}
