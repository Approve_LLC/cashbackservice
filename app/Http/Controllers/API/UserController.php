<?php

namespace App\Http\Controllers\API;

use App\Http\Resources\TransactionResource;
use App\Http\Resources\UserResource;
use App\Models\Transaction;
use App\Models\User;
use App\Repositories\UserRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Admitad;
use Illuminate\Contracts\Cache\Repository as Cache;

/**
 * @group User management
 *
 * APIs for managing users
 */
class UserController extends Controller
{

    private const CACHE_EXPIRE = 3600;

    /**
     * @var UserRepository
     */
    private $userRepository;

    /**
     * @var Cache
     */
    private $cache;

    /**
     * UserController constructor.
     * @param Cache $cache
     * @param UserRepository $userRepository
     */
    public function __construct(
        Cache $cache,
        UserRepository $userRepository
    )
    {
        $this->cache = $cache;
        $this->userRepository = $userRepository;
    }


    /**
     * @authenticated
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function show(Request $request)
    {
        return response()->json($request->user()->only([
            'id', 'name', 'email', 'created_at', 'referral_code', 'referral_id', 'gender', 'phone', 'was_born'
        ]), 200);
    }

    /**
     * @param Request $request
     * @return UserResource
     * @throws \Illuminate\Validation\ValidationException
     */
    public function update(Request $request): UserResource
    {
        $this->validate($request, [
            'name' => 'required|string',
            'email' => 'required|string|email|unique:users',
            'password' => 'required|string|confirmed',
            'gender' => 'string',
            'phone' => 'string',
            'was_born' => 'boolean',
            'device_token' => 'string|unique:users',
        ]);
        $id = $request->user()->id;
        $user = User::select([
            'name', 'email', 'password', 'referral_code', 'gender', 'phone', 'was_born', 'device_token'
        ])->where(['id' => $id])->first();
        $user->fill($request->input());
        $user->password = bcrypt($request->password ?? $user->password);
        $user->save();
        return new UserResource($user);
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function delete(Request $request): string
    {
        $id = $request->user()->id;
        User::where(['id' => $id])->delete();
        return response()->json([
            'message' => 'Пользователь удален!'
        ], 200);
    }

    /**
     * @param Request $request
     * @return TransactionResource
     * @throws \Illuminate\Validation\ValidationException
     */
    public function requestMoney(Request $request)
    {
        $this->validate($request, [
            'value' => 'required',
        ]);
        $id = $request->user()->id;
        $transaction = new Transaction();
        $transaction->user_id = $id;
        $transaction->value = $request->value;
        $transaction->status = Transaction::STATUS_PENDING;
        $transaction->save();

        return new TransactionResource($transaction);
    }

    /**
     * @param Request $request
     * @return string
     */
    public function getBalance(Request $request): string
    {
        $data = $this->userRepository->getBalance($request);

        return response()->json($data, 200);
    }

    /**
     * @param Request $request
     * @return string
     */
    public function getHistory(Request $request): string
    {
        $data = $this->userRepository->getHistory($request);

        return response()->json($data, 200);
    }

    /**
     * @param $platformId
     * @return string
     * @throws \Psr\SimpleCache\InvalidArgumentException
     */
    public function getPartnersByPlatform($platformId): string
    {
        $key = 'user.partnersByPlatform.' . $platformId;
        $data = $this->cache->get($key, false);

        if (false === $data) {
            $data = $this->userRepository->getPartnersByPlatform($platformId);
            $this->cache->set($key, $data, self::CACHE_EXPIRE);
        }

        return response()->json($data, 200);
    }

    /**
     * @return \Illuminate\Http\JsonResponse
     * @throws \Psr\SimpleCache\InvalidArgumentException
     */
    public function getPlatforms()
    {
        $key = 'user.platforms';
        $data = $this->cache->get($key, false);

        if (false === $data) {
            $data = $this->userRepository->getPlatforms();
            $this->cache->set($key, $data, self::CACHE_EXPIRE);
        }

        return response()->json($data, 200);
    }

    /**
     * @param Request $request
     * @param $platformId
     * @param $partnerId
     * @return \Illuminate\Http\JsonResponse
     */
    public function generateDeeplink(Request $request, $platformId, $partnerId): string
    {
        $data = $this->userRepository->generateDeeplink($request, $platformId, $partnerId);

        return response()->json($data, 200);
    }
}
