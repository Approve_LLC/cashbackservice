<?php

namespace App\Http\Middleware;

use App\Models\Setting;
use App\Services\AdServiceInterface;
use Closure;
use Admitad;

/**
 * Class AdmitadLogin
 * @package App\Http\Middleware
 */
class AdmitadLogin
{

    public function __construct(AdServiceInterface $admitadService)
    {
        $this->admitadService = $admitadService;
    }

    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \Closure $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $admitadApiToken = Setting::where(['name' => 'admitad_api_token'])->first();
        $admitadApiRefreshToken = Setting::where(['name' => 'admitad_api_refresh_token'])->first();
        $admitadApiTokenExpireAt = Setting::where(['name' => 'admitad_api_token_expire_at'])->first();
        if (empty($admitadApiToken->value)) {
            $response = $this->admitadService->getToken();
            if (empty($response)) {
                return response()->json([
                    'message' => 'Не удалось авторизоваться через Admitad.'
                ], 401);
            }
            $admitadApiToken->value = $response->getResult('access_token');
            $admitadApiToken->save();
            $admitadApiRefreshToken->value = $response->getResult('refresh_token');
            $admitadApiRefreshToken->save();
            $admitadApiTokenExpireAt->value = $response->getResult('expires_in') + time();
            $admitadApiTokenExpireAt->save();
        } else {
            if (true) {
                $response = $this->admitadService->refreshToken($admitadApiRefreshToken->value);
                if (empty($response)) {
                    return response()->json([
                        'message' => 'Не удалось авторизоваться через Admitad.'
                    ], 401);
                }
                $admitadApiToken->value = $response->getResult('access_token');
                $admitadApiToken->save();
                $admitadApiRefreshToken->value = $response->getResult('refresh_token');
                $admitadApiRefreshToken->save();
                $admitadApiTokenExpireAt->value = $response->getResult('expires_in') + time();
                $admitadApiTokenExpireAt->save();
            }
        }

        return $next($request);
    }
}
