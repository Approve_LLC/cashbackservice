<?php

namespace App\Repositories;

use Admitad;
use App\Models\Setting;
use App\Models\Transaction;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

/**
 * Class UserRepository
 * @package App\Repositories
 */
class UserRepository
{

    /**
     * @param Request $request
     * @return Admitad\Api\Model|array|mixed|null
     */
    public function getBalance(Request $request)
    {
        $token = Setting::where(['name' => 'admitad_api_token'])->first();
        $id = $request->user()->id;
        $api = new Admitad\Api\Api($token->value);
        $data = $api->get('/statistics/sub_ids/', [
            'subid' => [
                $id
            ]
        ])->getResult();
        $paymentSumApproved = $data->results->payment_sum_approved;
        $paymentSumApproved = (config('app.cashback_percent') / 100) * $paymentSumApproved;
        $paymentSumApproved = $paymentSumApproved - $request->user()
                ->transactions()
                ->where(['status' => Transaction::STATUS_SUCCESS])
                ->sum('value');
        $paymentSumOpen = $data->results->payment_sum_open;
        $paymentSumOpen = (config('app.cashback_percent') / 100) * $paymentSumOpen;
        $data = [
            'payment_sum_all' => $paymentSumApproved + $paymentSumOpen,
            'payment_sum_approved' => $paymentSumApproved,
            'payment_sum_open' => $paymentSumOpen
        ];

        return $data;
    }

    /**
     * @param Request $request
     * @return array
     */
    public function getHistory(Request $request)
    {
        $token = Setting::where(['name' => 'admitad_api_token'])->first();
        $api = new Admitad\Api\Api($token->value);
        $id = $request->user()->id;
        $data = $api->get('/statistics/actions/', array(
            'subid' => [
                $id
            ]
        ))->getResult();
        $response = [];
        foreach ($data->results as $result) {
            $status = $result->status;
            $payment = $result->payment;
            $payment = (config('app.cashback_percent') / 100) * $payment;
            $createdAt = $result->action_date;
            $orderId = $result->order_id;
            $name = $result->advcampaign_name;
            $cost = $result->cart;
            array_push($response, [
                'status' => $status,
                'payment' => $payment,
                'createdAt' => $createdAt,
                'orderId' => $orderId,
                'name' => $name,
                'cost' => $cost
            ]);
        }

        return $response;
    }

    /**
     * @param $platformId
     * @return array
     */
    public function getPartnersByPlatform($platformId)
    {
        $token = Setting::where(['name' => 'admitad_api_token'])->first();
        $api = new Admitad\Api\Api($token->value);
        $data = $api->get('/advcampaigns/website/' . $platformId . '/', [])->getResult();
        $response = [];
        foreach ($data->results as $result) {
            $status = $result->connection_status;
            if (!empty($status) && $status == 'active') {
                array_push($response, [
                    'id' => $result->id,
                    'name' => $result->name,
                    'site_url' => $result->site_url,
                    'allow_deeplink' => $result->allow_deeplink,
                ]);
            }
        }

        return $response;
    }

    /**
     * @return array
     */
    public function getPlatforms()
    {
        $token = Setting::where(['name' => 'admitad_api_token'])->first();
        $api = new Admitad\Api\Api($token->value);
        $data = $api->get('/websites/', [])->getResult();
        $response = [];
        foreach ($data->results as $result) {
            $status = $result->status;
            if (!empty($status) && $status == 'active') {
                array_push($response, [
                    'id' => $result->id,
                    'name' => $result->name,
                    'site_url' => $result->site_url
                ]);
            }
        }

        return $response;
    }

    /**
     * @param Request $request
     * @param $platformId
     * @param $partnerId
     * @return Admitad\Api\Model|\Illuminate\Http\JsonResponse|mixed|null
     */
    public function generateDeeplink(Request $request, $platformId, $partnerId)
    {
        $validator = Validator::make($request->all(), [
                'ulp' => 'required',
            ]
        );
        if ($validator->fails()) {
            $messages = $validator->messages();
            return response()->json([
                'message' => $messages
            ], 400);
        }
        $token = Setting::where(['name' => 'admitad_api_token'])->first();
        $api = new Admitad\Api\Api($token->value);
        $subid = $request->user()->id;
        $data = $api->get('/deeplink/' . $platformId . '/advcampaign/' . $partnerId . '/', [
            'subid' => $subid,
            'ulp' => $request->ulp
        ])->getResult();

        return $data;
    }
}
