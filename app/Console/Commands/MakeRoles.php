<?php

namespace App\Console\Commands;

use App\Models\Permission;
use App\Models\Role;
use App\Models\User;
use Illuminate\Console\Command;

class MakeRoles extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'roles:create';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Create roles to users and give them permissions';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $admin = new Role();
        $admin->name         = 'admin';
        $admin->display_name = 'User Administrator'; // optional
        $admin->description  = 'User is allowed to manage and edit other users'; // optional
        $admin->save();

        $moderator = new Role();
        $moderator->name         = 'moderator';
        $moderator->display_name = 'Moderator'; // optional
        $moderator->description  = 'Moderator'; // optional
        $moderator->save();

        $userRole = new Role();
        $userRole->name         = 'user';
        $userRole->display_name = 'User'; // optional
        $userRole->description  = 'User'; // optional
        $userRole->save();

        $users = User::all();
        foreach ($users as $user) {
            $user->attachRole($userRole);
        }

        $adminFirst = new User([
            'name' => 'Alex',
            'email' => 'alex@gmail.com',
            'password' => bcrypt('pleasechangeme')
        ]);
        $adminFirst->save();
        $adminFirst->attachRole($admin);

        $adminSecond = new User([
            'name' => 'Mike',
            'email' => 'mike@gmail.com',
            'password' => bcrypt('pleasechangemetoo')
        ]);
        $adminSecond->save();
        $adminSecond->attachRole($admin);

        $adminPermission = new Permission();
        $adminPermission->name         = 'admin-permission';
        $adminPermission->display_name = 'Admin permission'; // optional
        $adminPermission->description  = 'Admin permission'; // optional
        $adminPermission->save();
        $admin->attachPermission($adminPermission);

        $moderatorPermission = new Permission();
        $moderatorPermission->name         = 'moderator-permission';
        $moderatorPermission->display_name = 'Moderator permission'; // optional
        $moderatorPermission->description  = 'Moderator permission'; // optional
        $moderatorPermission->save();
        $moderator->attachPermission($moderatorPermission);
    }
}
