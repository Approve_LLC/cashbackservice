<?php

use Illuminate\Database\Seeder;

class SettingsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('settings')->insert([
            [
                'name' => 'admitad_api_token',
                'value' => ''
            ],
            [
                'name' => 'admitad_api_refresh_token',
                'value' => ''
            ],
            [
                'name' => 'admitad_api_token_expire_at',
                'value' => ''
            ],
            [
                'name' => 'android_notification',
                'value' => 'Hello! We wait you back!'
            ],
            [
                'name' => 'ios_notification',
                'value' => 'Hello! We wait you back!'
            ],
        ]);
    }
}
