<?php

use Kreait\Firebase\Factory;
use Kreait\Firebase\Messaging\CloudMessage;
use Kreait\Firebase\Messaging\Notification;
use Kreait\Firebase\ServiceAccount;

Route::get('/', function () {
    return redirect('admin');
});

Route::get('/send-android', function () {
    $data = [
        'first_key' => 'Проверка',
        'second_key' => 'Вит, привет!',
    ];
    $title = 'Notification Проверка';
    $body = 'My Notification Body';
    $notification = Notification::create($title, $body);
    $topic = 'a-topic';
    $deviceToken = 'fWLv3f-9hv0:APA91bEL0usw2rQPNph37xx_I0fmyDB_kjjPNuUBAncGDNP5URtvGjyO7ZfQckQIRIjDE5Xo4sRSeDqs_-IIKLMM2SxamkIEMAyWtGwigWrUa5RS1D4AL3_CVfYjInGw1N727qMGX4A9';
    $message = CloudMessage::
    withTarget('token', $deviceToken)
    //withTarget('topic', $topic)
        ->withNotification($notification)// optional
        ->withData($data);
    $serviceAccount = ServiceAccount::fromJsonFile(__DIR__.'/../config/cashback-232317-firebase-adminsdk-2ftol-3a4c5b2b06.json');
    $firebase = (new Factory)
        ->withServiceAccount($serviceAccount)
        ->create();
    $messaging = $firebase->getMessaging();
    var_dump($messaging->send($message));
});

Route::get('/send-ios', function () {
});

Auth::routes();
