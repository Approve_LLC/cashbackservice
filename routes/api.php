<?php


/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

use Illuminate\Support\Facades\Route;

Route::group([
    'prefix' => 'auth'
], function () {
    Route::post('login', 'API\AuthController@login');
    Route::post('signup', 'API\AuthController@signup');

    Route::get('fb', 'API\AuthController@redirectToFbProvider');
    Route::get('fb/callback', 'API\AuthController@handleProviderFbCallback');

    Route::get('google', 'API\AuthController@redirectToGoogleProvider');
    Route::get('google/callback', 'API\AuthController@handleProviderGoogleCallback');

    Route::get('vk', 'API\AuthController@redirectToVkProvider');
    Route::get('vk/callback', 'API\AuthController@handleProviderVkCallback');

    Route::post('login-by-mail', 'API\AuthController@loginByMail');
    Route::get('login-by-mail-callback', 'API\AuthController@loginByMail');

    Route::group([
        'middleware' => 'auth:api'
    ], function () {
        Route::get('logout', 'API\AuthController@logout');

    });
});

Route::group([
    'middleware' => 'auth:api'
], function () {
    Route::get('users', 'API\UserController@show');
    Route::get('users/balance', 'API\UserController@getBalance')->middleware('admitad.login');
    Route::get('users/history', 'API\UserController@getHistory')->middleware('admitad.login');
    Route::get('users/platforms', 'API\UserController@getPlatforms')->middleware('admitad.login');
    Route::get('users/platforms/{platformId}/partners', 'API\UserController@getPartnersByPlatform')->middleware('admitad.login');
    Route::get('users/{platformId}/deeplink/{partnerId}', 'API\UserController@generateDeeplink')->middleware('admitad.login');
    Route::put('users', 'API\UserController@update');
    Route::delete('users', 'API\UserController@delete');
    Route::post('users/request', 'API\UserController@requestMoney');
});

Route::group([
    'middleware' => 'auth:api'
], function () {
    Route::get('categories', 'API\CategoryController@index');
    Route::post('categories', 'API\CategoryController@create');
    Route::get('categories/{id}', 'API\CategoryController@show');
    Route::get('categories/{categoryId}/shops', 'API\CategoryController@listByCategory');
});

Route::group([
    'middleware' => 'auth:api'
], function () {
    Route::get('shops', 'API\ShopController@index');
    Route::get('shops/{id}', 'API\ShopController@show');
});

Route::group([
    'middleware' => 'auth:api'
], function () {
    Route::post('transactions', 'API\TransactionController@create');
    Route::delete('transactions/{id}', 'API\TransactionController@delete');
});

Route::group([
    'middleware' => 'auth:api'
], function () {
    Route::post('notifications/{id}/send', 'API\NotificationController@send')->middleware('role:admin');
});