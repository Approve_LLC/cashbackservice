<?php

return [
    'output' => 'public/docs',
    'router' => 'laravel',
    'postman' => true,
    'routes' => [
        [
            'match' => [
                'domains' => [
                    '*',
                ],
                'prefixes' => [
                    'api/*',
                ],
            ],
            'include' => [],
            'exclude' => ['auth.*'],
            'apply' => [
                'headers' => [
                    'Authorization' => 'Bearer: {token}',
                    // 'Api-Version' => 'v2',
                ],
                'response_calls' => [
                    'methods' => ['GET'],
                    'bindings' => [
                        // '{user}' => 1
                    ],

                    /*
                     * Environment variables which should be set for the API call.
                     * This is a good place to ensure that notifications, emails
                     * and other external services are not triggered during the documentation API calls
                     */
                    'env' => [
                        'APP_ENV' => 'documentation',
                        'APP_DEBUG' => false,
                        // 'env_var' => 'value',
                    ],

                    /*
                     * Headers which should be sent with the API call.
                     */
                    'headers' => [
                        'Content-Type' => 'application/json',
                        'Accept' => 'application/json',
                        // 'key' => 'value',
                    ],

                    /*
                     * Query parameters which should be sent with the API call.
                     */
                    'query' => [
                        // 'key' => 'value',
                    ],

                    /*
                     * Body parameters which should be sent with the API call.
                     */
                    'body' => [
                        // 'key' => 'value',
                    ],
                ],
            ],
        ],
    ],

    /*
     * Custom logo path. Will be copied during generate command. Set this to false to use the default logo.
     *
     * Change to an absolute path to use your custom logo. For example:
     * 'logo' => resource_path('views') . '/api/logo.png'
     *
     * If you want to use this, please be aware of the following rules:
     * - size: 230 x 52
     */
    'logo' => false,
];
